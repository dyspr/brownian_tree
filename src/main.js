var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 101
var array = create2DArray(arraySize, arraySize, 0, true)
var initSize = 0.008
var aggregate = [[Math.floor(arraySize * 0.5), Math.floor(arraySize * 0.5)]]
var numOfParticles = 2000 + Math.floor(Math.random() * 2000)
var particlesRemaining = numOfParticles
var particles = []
array[aggregate[0][0]][aggregate[0][1]] = 0.001
var possiblePositions = []
var neighbours = []
var clusterDetected = false
var frame = 0
var growState = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] === 0) {
        possiblePositions.push([i, j])
      }
    }
  }
  possiblePositions = shuffle(possiblePositions)
  particles = possiblePositions.splice(0, numOfParticles)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      fill(64 + (255 - 64) * array[i][j])
      noStroke()
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * initSize)
      if (array[i][j] !== 0) {
        rect(0, 0, boardSize * initSize * 1.05, boardSize * initSize * 1.05)
      }
      pop()
    }
  }

  fill(255)
  noStroke()
  for (var i = 0; i < particles.length; i++) {
    push()
    translate(windowWidth * 0.5 + (particles[i][0] - Math.floor(arraySize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (particles[i][1] - Math.floor(arraySize * 0.5)) * boardSize * initSize)
    rect(0, 0, boardSize * initSize * 0.125, boardSize * initSize * 0.125)
    pop()
  }

  frame += deltaTime * 0.025
  if (frame > 1) {
    frame = 0

    for (var i = 0; i < particles.length; i++) {
      clusterDetected = clusterDetection(particles[i], array)
      if (clusterDetected === true) {
        array[particles[i][0]][particles[i][1]] = 1 - (1 / numOfParticles) * particlesRemaining
        particles.splice(i, 1)
        particlesRemaining--
      } else {
        neighbours = getNeighbours(particles[i], array)
        particles[i] = neighbours[Math.floor(Math.random() * neighbours.length)]
        neighbours = []
      }
      clusterDetected = false
    }
    if (particles.length === 0) {
      setTimeout(function() {
        numOfParticles = 2000 + Math.floor(Math.random() * 2000)
        array = create2DArray(arraySize, arraySize, 0, true)
        aggregate = [[Math.floor(arraySize * 0.5), Math.floor(arraySize * 0.5)]]
        particlesRemaining = numOfParticles
        particles = []
        array[aggregate[0][0]][aggregate[0][1]] = 0.001
        possiblePositions = []
        neighbours = []
        clusterDetected = false
        for (var i = 0; i < array.length; i++) {
          for (var j = 0; j < array[i].length; j++) {
            if (array[i][j] === 0) {
              possiblePositions.push([i, j])
            }
          }
        }
        possiblePositions = shuffle(possiblePositions)
        particles = possiblePositions.splice(0, numOfParticles)
        growState++
        if (growState > 3) {
          growState = 0
        }
      }, 1000)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function clusterDetection(particle, arr) {
  var state = false
  if (growState === 0) {
    // adjacent neighbours
    if (particle[1] - 1 !== -1) {
      if (arr[particle[0]][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] - 1 !== -1) {
      if (arr[particle[0] - 1][particle[1]] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize) {
      if (arr[particle[0] + 1][particle[1]] !== 0) {
        state = true
      }
    }
    if (particle[1] + 1 !== arraySize) {
      if (arr[particle[0]][particle[1] + 1] !== 0) {
        state = true
      }
    }
    // diagonal neighbours
    if (particle[0] - 1 !== -1 && particle[1] - 1 !== -1) {
      if (arr[particle[0] - 1][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize && particle[1] - 1 !== -1) {
      if (arr[particle[0] + 1][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] - 1 !== -1 && particle[1] + 1 !== arraySize) {
      if (arr[particle[0] - 1][particle[1] + 1] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize && particle[1] + 1 !== arraySize) {
      if (arr[particle[0] + 1][particle[1] + 1] !== 0) {
        state = true
      }
    }
  }
  if (growState === 1) {
    // diagonal neighbours
    if (particle[0] - 1 !== -1 && particle[1] - 1 !== -1) {
      if (arr[particle[0] - 1][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize && particle[1] - 1 !== -1) {
      if (arr[particle[0] + 1][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] - 1 !== -1 && particle[1] + 1 !== arraySize) {
      if (arr[particle[0] - 1][particle[1] + 1] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize && particle[1] + 1 !== arraySize) {
      if (arr[particle[0] + 1][particle[1] + 1] !== 0) {
        state = true
      }
    }
  }
  if (growState === 2) {
    // adjacent neighbours
    if (particle[1] - 1 !== -1) {
      if (arr[particle[0]][particle[1] - 1] !== 0) {
        state = true
      }
    }
    if (particle[0] - 1 !== -1) {
      if (arr[particle[0] - 1][particle[1]] !== 0) {
        state = true
      }
    }
    if (particle[0] + 1 !== arraySize) {
      if (arr[particle[0] + 1][particle[1]] !== 0) {
        state = true
      }
    }
    if (particle[1] + 1 !== arraySize) {
      if (arr[particle[0]][particle[1] + 1] !== 0) {
        state = true
      }
    }
  }
  return state
}

function getNeighbours(particle, arr) {
  var neighbours = []
  // adjacent neighbours
  if (particle[1] - 1 !== -1) {
    neighbours.push([particle[0], particle[1] - 1])
  }
  if (particle[0] - 1 !== -1) {
    neighbours.push([particle[0] - 1, particle[1]])
  }
  if (particle[0] + 1 !== arraySize) {
    neighbours.push([particle[0] + 1, particle[1]])
  }
  if (particle[1] + 1 !== arraySize) {
    neighbours.push([particle[0], particle[1] + 1])
  }
  // diagonal neighbours
  if (particle[0] - 1 !== -1 && particle[1] - 1 !== -1) {
    neighbours.push([particle[0] - 1, particle[1] - 1])
  }
  if (particle[0] + 1 !== arraySize && particle[1] - 1 !== -1) {
    neighbours.push([particle[0] + 1, particle[1] - 1])
  }
  if (particle[0] - 1 !== -1 && particle[1] + 1 !== arraySize) {
    neighbours.push([particle[0] - 1, particle[1] + 1])
  }
  if (particle[0] + 1 !== arraySize && particle[1] + 1 !== arraySize) {
    neighbours.push([particle[0] + 1, particle[1] + 1])
  }
  return neighbours
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
